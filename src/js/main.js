(function (window, _) {
  window.template = function(id) {
    return _.template($('#' + id).html());
  };

  var App = window.App = _.extend({}, {
    Views: {},
    Models: {},
    Collections: {}
  }, window.App);

  App.Models.Task = Backbone.Model.extend({
    defaults: {
      title: null,
    },
  });

  App.Views.Task = Marionette.ItemView.extend({

    template: '#task-template',

    events: {
      'click .js-task-remove' : 'onRemoveClick',
    },

    onRemoveClick: function(e) {
      e.preventDefault();
      this.model.destroy();
    },
  });

  App.Collections.Tasks = Backbone.Collection.extend({
    model: App.Models.Task,
  });

  App.Views.Tasks = Marionette.CollectionView.extend({
    el: '#tasks',

    childView: App.Views.Task,
  });

  App.Views.TaskForm = Backbone.View.extend({
    events: {
      'submit form' : 'onFormSubmit',
    },

    onFormSubmit: function (e) {
      e.preventDefault();

      var title = this.$('#task-title').val();
      var task = new App.Models.Task({title: title});
      this.collection.add(task);

      this.$('form')[0].reset();
    }
  });

})(window, _);
